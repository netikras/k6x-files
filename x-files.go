package xfiles

import (
	"go.k6.io/k6/js/modules"
	"os/user"
)
import (
	"fmt"
	"os"
	"path/filepath"
)

func init() {
	modules.Register("k6/x/files", new(FileDef))
}

type Error struct {
	message string
}

func (e Error) Error() string {
	panic(e.message)
	return e.message
}

type FileOwner struct {
	user  string
	group string
}

type FilePermission struct {
	read  bool
	write bool
	exec  bool
}

type FileDef struct {
	name    string
	path    string
	size    uint
	ftype   string
	members []string

	owner FileOwner

	mode  uint16 // Using uint16 for file mode
	permU FilePermission
	permG FilePermission
	permO FilePermission
}

func (fd *FileDef) canRead() bool {
	return fd.permU.read
}

func (fd *FileDef) canWrite() bool {
	return fd.permU.write
}

func (fd *FileDef) canExec() bool {
	return fd.permU.exec
}

func (fd *FileDef) create(recursive bool) error {
	if recursive {
		// Create parent directories recursively if needed
		err := os.MkdirAll(fd.parent().path, os.ModePerm)
		if err != nil {
			return err
		}
	}

	if fd.ftype == "d" {
		// Create a directory
		return os.Mkdir(fd.path, os.ModePerm)
	} else if fd.ftype == "f" {
		// Create a file
		file, err := os.Create(fd.path)
		if err != nil {
			return err
		}
		defer file.Close()
	} else {
		return Error{"Unknown file type (ftype)"}
	}

	file, err := getFile(fd.path)
	if err != nil {
		return err
	}

	file.cloneTo(fd)

	return nil
}

func (fd *FileDef) cloneTo(target *FileDef) *FileDef {
	if target == nil {
		target = &FileDef{}
	}
	target.ftype = fd.ftype
	target.path = fd.path
	target.name = fd.name
	target.size = fd.size
	target.mode = fd.mode
	target.owner = fd.owner
	target.permU = fd.permU
	target.permG = fd.permG
	target.permO = fd.permO
	target.members = fd.members
	return target
}

func (fd *FileDef) parent() *FileDef {
	parentPath := filepath.Dir(fd.path)

	return &FileDef{
		path: parentPath,
	}
}

func (fd *FileDef) isType(t string) bool {
	return t == fd.ftype
}

func getFile(filePath string) (*FileDef, error) {
	fd := FileDef{}
	file, err := os.Stat(filePath)
	if err != nil {
		return nil, err
	}

	fd.name = file.Name()
	fd.path = filePath
	fd.size = uint(file.Size())

	mode := file.Mode()
	if mode.IsDir() {
		fd.ftype = "d"
		entries, _ := os.ReadDir(filePath)
		for _, entry := range entries {
			fd.members = append(fd.members, entry.Name())
		}
	} else {
		fd.ftype = "f"
	}

	fd.mode = uint16(mode.Perm())

	//fd.owner = FileOwner{
	//	user:  "john",
	//	group: "users",
	//}

	fd.permU = FilePermission{
		read:  fd.mode&0x0400 != 0,
		write: fd.mode&0x0200 != 0,
		exec:  fd.mode&0x0100 != 0,
	}
	fd.permG = FilePermission{
		read:  fd.mode&0x0040 != 0,
		write: fd.mode&0x0020 != 0,
		exec:  fd.mode&0x0010 != 0,
	}
	fd.permO = FilePermission{
		read:  fd.mode&0x0004 != 0,
		write: fd.mode&0x0002 != 0,
		exec:  fd.mode&0x0001 != 0,
	}
	return &fd, nil
}

func makeFile(ftype, path string, mode uint16) (*FileDef, error) {
	currentUser, usrErr := user.Current()
	if usrErr != nil {
		return nil, usrErr
	}
	group, grErr := user.LookupGroupId(currentUser.Gid)
	if grErr != nil {
		return nil, grErr
	}

	file := FileDef{
		ftype: ftype,
		path:  path,
		name:  filepath.Base(path),
		owner: FileOwner{
			user:  currentUser.Username,
			group: group.Name,
		},
		mode: mode,
	}

	return &file, nil
}

func (fd *FileDef) exists() bool {
	stat, _ := os.Stat(fd.path)
	return stat != nil
}

func main() {
	// Example: Getting file metadata
	file, err := makeFile("f", "/tmp/path/to/your/file", 0o664)

	fmt.Println(err)

	fmt.Printf("Decimal value: %d\n", file.mode)
	fmt.Printf("Octal value : %o\n", file.mode)
	fmt.Printf("Octal value : %O\n", file.mode)

	// Printing the updated file definition
	fmt.Printf("File Definition:\n%+v\n", file)
	err = file.create(true)
	fmt.Println(err)

	file, err = getFile("/tmp/playground")
	fmt.Printf("Decimal value: %d\n", file.mode)
	fmt.Printf("Octal value : %o\n", file.mode)
	fmt.Printf("Octal value : %O\n", file.mode)
	fmt.Printf("File Definition:\n%+v\n", file)

}
